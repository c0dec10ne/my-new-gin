package mysqlDB

import (
	"fmt"
	"my-new-gin/configs"
	"sync"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	DB   *gorm.DB
	once sync.Once
)

func GetDBConnection(mysqlConfig configs.MysqlConf) (*gorm.DB, error) {
	var err error
	once.Do(func() {
		DB, err = gorm.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=true&loc=Local", mysqlConfig.Username, mysqlConfig.Password, mysqlConfig.Ip, mysqlConfig.Port, mysqlConfig.DB))
		DB.LogMode(true)
		DB.DB().SetMaxIdleConns(2000)
		DB.DB().SetMaxOpenConns(1000)
	})
	return DB, err
}

func DBConnectionClose() {
	DB.Close()
}
