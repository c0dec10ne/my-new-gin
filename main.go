package main

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/redis"
	"github.com/gin-gonic/gin"
	"my-new-gin/configs"
	"my-new-gin/middleware"
	"my-new-gin/routers"
	_ "my-new-gin/schedule"
)

func main() {
	route := gin.Default()
	redisConf := configs.GetRedisConf()
	store, _ := redis.NewStore(10, "tcp", redisConf.Ip+":"+redisConf.Port, redisConf.Password, []byte("secret"))
	route.Use(sessions.Sessions("mysession", store))
	//route.Use(middleware.Logger())
	route.Use(middleware.Cors())

	routers.RegisterRoutes(route)

	appConf := configs.GetAppConf()
	err := route.Run(appConf.Ip+":"+appConf.Port)
	if err != nil {
		panic(err)
	}
}
