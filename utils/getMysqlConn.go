package utils

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"my-new-gin/configs"
	"my-new-gin/dbs/mysqlDB"
)

func GetMysqlConn() (*gorm.DB, error) {
	db, err := mysqlDB.GetDBConnection(configs.GetMysqlConf())
	if err != nil {
		fmt.Printf("server error -> %s",err.Error())
		return nil, errors.New("service error")
	}
	return db, nil
}
