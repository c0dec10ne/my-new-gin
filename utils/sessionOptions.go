package utils

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func SetSession(ctx *gin.Context,key interface{}, value interface{})  {
	session := sessions.Default(ctx)
	session.Set(key,value)
}

func GetSession(ctx *gin.Context,key interface{}) interface{} {
	session := sessions.Default(ctx)
	return session.Get(key)
}