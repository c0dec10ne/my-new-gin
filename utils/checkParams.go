package utils
//检查map中的key是否都存在，如果缺少某些键就返回出去
func CheckParams(keys []string, mp map[string]interface{}) ([]string, bool) {
	flag := true
	arr := make([]string,0)
	for _, k := range keys {
		_, ok := mp[k]
		flag = flag && ok
		if !ok {
			arr = append(arr, k)
		}
	}
	return arr, flag
}
