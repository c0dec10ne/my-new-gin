package utils

import (
	"encoding/json"
	"fmt"
)

func Map2Json(m map[string]interface{}) (string, error) {
	bt, err := json.Marshal(m)
	if err != nil {
		return "", fmt.Errorf("Map To Json ERROR: %v \n",err)
	}
	return string(bt), nil
}
