package utils

import (
	"errors"
	"fmt"
	"github.com/go-redis/redis"
	"my-new-gin/configs"
	"my-new-gin/dbs/redisDB"
)

func GetRedisClient() (*redis.Client, error) {
	cli, err := redisDB.GetRedisClient(configs.GetRedisConf())
	if err != nil {
		fmt.Printf("server error -> %s",err.Error())
		return nil, errors.New("service error")
	}
	return cli, nil
}
