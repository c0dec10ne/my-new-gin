package utils

import (
	"encoding/json"
	"fmt"
)

func Struct2Json(st interface{}) (string, error) {
	jsonBt, err := json.Marshal(st)
	if err != nil {
		return "", fmt.Errorf("Struct To Json ERROR: %v \n",err)
	}
	return string(jsonBt), nil
}
