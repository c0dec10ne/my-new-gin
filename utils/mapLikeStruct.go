package utils

import (
	"reflect"
)
//数据库插入的时候整理参数跟结构体定义的数据保持一致，避免出错
func MapLikeStruct(mp map[string]interface{},st interface{}) map[string]interface{}  {
	cType := reflect.TypeOf(st)

	cValue := reflect.ValueOf(st)

	structLen := cValue.Elem().NumField()

	structMap := make(map[string]interface{}, structLen)

	for i := 0; i < structLen; i++ {
		field := cType.Elem().Field(i)
		val, ok := mp[field.Tag.Get("orm")]
		if ok {
			structMap[field.Tag.Get("orm")] = val
		}
	}
	return structMap
}
